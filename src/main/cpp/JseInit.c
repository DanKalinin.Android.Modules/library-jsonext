//
// Created by Dan on 17.11.2021.
//

#include "JseInit.h"

jint JNI_OnLoad(JavaVM *vm, gpointer reserved) {
    jse_init();

    return JNI_VERSION_1_2;
}

void JNI_OnUnload(JavaVM *vm, gpointer reserved) {

}
